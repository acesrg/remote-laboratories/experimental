\htmlonly
<center><a href="https://gitlab.com/marcomiretti/remote-control-lab"><img src="https://user-images.githubusercontent.com/26353057/82385781-43bc0480-9a09-11ea-87a9-e8fedc28413b.png" alt="remote control lab" width="800"/></a></center>
\endhtmlonly

The remote control lab is a control-systems *remote* laboratory. It's main goal is to aid students in the understanding of multiple control techniques, and allow them experimentally to manipulate and control multiple dynamic systems.

# About this repo: Experimental lab
This repo contains the firmware for the laboratory "Experimental", we use it to test and develop new features like:
- The communication with the sensors and actuators (read and write, respectively).
- Communication libs for HTTP or Websockets, to set and get the formerly mentioned data.

# Useful Links

- [The API](https://acesrg.gitlab.io/remote-laboratories/experimental/openapi.html)
- [Gitlab repo](https://gitlab.com/acesrg/remote-laboratories/experimental)
