# Developing from scratch

# Requirements

Quite simple, you will need: 
- make
- docker
- a machine with either _amd64_ or _arm64_ architecture

# Walking into the containerized development environment

1. Clone this repo.
```bash
git clone https://gitlab.com/acesrg/remote-laboratories/experimental.git
```
2. Export your local wifi network credentials.
```bash
export WIFI_SSID=YOURSSID
export WIFI_PASS=YOURPASS
```
3. `cd` into the repo.
```bash
cd <the-repo>/
```
4. Run the container launcher
```bash
tools/dockeride
```
5. Start developing!

# Building and flashing

```bash
make
```

```bash
make flash
```

# Testing

```bash
make unittest
```
