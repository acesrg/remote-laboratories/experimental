/*
 * Copyright 2021 Marco Miretti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file resistance_cgi.c */
#include <string.h>
#include <stdlib.h>

#include <httpd/httpd.h>
#include <log.h>

#include <cgi_utils.h>
#include <ssi_utils.h>

#include <resistance_cgi.h>
#include <resistance_utils.h>

extern uint16_t RES_VALUE;

const char *resistance_value_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[]) {
    uriParamsType resistance_cgi_params[2] = {{"verb", "0", false},
                                             {"value", "0", false}};

    retval_t rv;

    rv = decode_uri_params(
        iNumParams,
        (char **) pcParam,
        (char **) pcValue,
        sizeof(resistance_cgi_params) / sizeof(resistance_cgi_params[0]),
        resistance_cgi_params);

    if (rv != RV_OK) {
        return HTTP_CODE(500);
    }

    char param_verb[URI_VARIABLE_VALUE_MAX_LEN];

    rv = uri_param_read(
            "verb",
            param_verb,
            resistance_cgi_params,
            sizeof(resistance_cgi_params) / sizeof(resistance_cgi_params[0]));

    if (rv != RV_OK) {
        return HTTP_CODE(400);
    }

    return (char *) handle(resistance_cgi_params, param_verb);
}
