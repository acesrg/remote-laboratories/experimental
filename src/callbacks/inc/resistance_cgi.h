/*
 * Copyright 2021 Marco Miretti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file resistance_cgi.h */
#ifndef SRC_CALLBACKS_INC_RESISTANCE_CGI_H_
#define SRC_CALLBACKS_INC_RESISTANCE_CGI_H_

/*
 * Resistance definitions
 **/
#define MAX_RES_VALUE     10000   /**< \brief Arbitrary max telemetry period */
#define MIN_RES_VALUE     10       /**< \brief Min telemetry period (sw limitation) */

/**
 * \brief   A handler for the resistance_value resource, allows to set and read it.
 * \remark  Endpoint is http://$HOST_IP/telemetry/period
 * \param   iIndex: Unused.
 * \param   iNumParams: Quantity of variables in the query.
 * \param   *pcParam[]: Pointer to first char of first string in query-names array.
 * \param   *pcValue[]: Pointer to first char of first string in query-values array.
 * \return  A file name, which will contain an HTTP return code.
 */
const char *resistance_value_cgi_handler(int iIndex, int iNumParams, char *pcParam[], char *pcValue[]);

#endif /* SRC_CALLBACKS_INC_RESISTANCE_CGI_H_ */
