/*
 * Copyright 2021 ACES.
 *third party local libsin the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file potentiometer_dwarf.c */
/* standard */
/* third party libs */
#include <espressif/esp_common.h>
#include <esp/uart.h>
#include <FreeRTOS.h>
#include <task.h>
#include <esp8266.h>
#include <stdio.h>
#include <esp/spi.h>
#include <spi_res_writer.h>

/* parameter assignment for RESISTANCE */
#define SIZE_RES_VALUES                    9       /**< \brief Array size of resistor values */
#define DEFAULT_RES_VALUE                  5000

/* global variable: integer */
uint16_t res_v[SIZE_RES_VALUES] = {7000, 1000, 7000, 1000, 7000, 1000, 7000, 1000, 7000};
uint16_t RES_VALUE = DEFAULT_RES_VALUE;

void spi_res(void *pvParameters) {
    spi_init(1, SPI_MODE0, SPI_FREQ_DIV_1M, 1,  SPI_BIG_ENDIAN, false);  // init SPI module

while (1) {
        for (int i = 0; i < SIZE_RES_VALUES; i ++) {
        mcp41010_set_resistance_value(res_v[i]);
        vTaskDelay(20000/portTICK_PERIOD_MS);
        }
    }
}
