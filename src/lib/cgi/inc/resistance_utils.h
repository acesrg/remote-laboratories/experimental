/*
 * Copyright 2021 Marco Miretti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file resistance_utiles.h */
#ifndef SRC_LIB_CGI_INC_RESISTANCE_UTILS_H_
#define SRC_LIB_CGI_INC_RESISTANCE_UTILS_H_

#include <string.h>
#include <stdlib.h>

#include <httpd/httpd.h>
#include <log.h>

#include <cgi_utils.h>
#include <ssi_utils.h>

#include <retval.h>
#include <resistance_cgi.h>


const char *handle(uriParamsType *uri_params, const char *param_verb);
#endif  // SRC_LIB_CGI_INC_RESISTANCE_UTILS_H_
