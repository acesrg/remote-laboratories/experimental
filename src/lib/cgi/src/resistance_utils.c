/*
 * Copyright 2021 Marco Miretti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file resistance_utiles.c */
#include <resistance_utils.h>
#include <string.h>
#include <stdlib.h>

#include <httpd/httpd.h>
#include <log.h>

#include <cgi_utils.h>
#include <ssi_utils.h>

#include <retval.h>

extern uint16_t RES_VALUE;

const char *handle(uriParamsType *uri_params, const char *param_verb) {
  retval_t rv;
  switch (decode_http_verb(param_verb)) {
        case GET: {
            int response_len;
            char response[LWIP_HTTPD_MAX_TAG_INSERT_LEN];
            response_len = sprintf(response, "%d", RES_VALUE);
            load_ssi_data(response, response_len + 1);
            return "/response.ssi";
        }

        case POST: {
            char param_value[URI_VARIABLE_VALUE_MAX_LEN];

            rv = uri_param_read(
                    "value",
                    param_value,
                    uri_params,
                    sizeof(uri_params) / sizeof(uri_params[0]));

            if (rv != RV_OK) {
  return HTTP_CODE(400);
            }

            int value_int = atoi(param_value);

            if ((value_int > MAX_RES_VALUE) || (value_int < MIN_RES_VALUE)) {
                log_error("Res of %d Ohm is out of bounds.\n"
  "(max: %d Ohm, min: %d Ohm), keeping %d Ohm.", value_int, MAX_RES_VALUE, MIN_RES_VALUE, RES_VALUE);
                return HTTP_CODE(403);
            } else {
                log_info("Changing resistance value to: %d [Ohm]", value_int);
                RES_VALUE = value_int;
            }
            return HTTP_CODE(202);

        default:
            return HTTP_CODE(400);
        }
    }
}
