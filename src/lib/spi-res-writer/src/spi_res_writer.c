/*
 * Copyright 2020 Marco Miretti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file spi_res_writer.c */

#include <espressif/esp_common.h>
#include <esp/uart.h>
#include <FreeRTOS.h>
#include <task.h>
#include <esp8266.h>
#include <stdio.h>
#include <esp/spi.h>

#include <spi_res_writer.h>

void mcp41010_set_resistance_value(int resistance) {
    uint8_t res;
    res = (((resistance *256)/10000)* 0xFF) / 256;
    spi_set_command(1, 8, 0x13);  // Set 8 command bits to write in both potenciometers
    spi_repeat_send_8(1, res, 1);  // Send 8 bits command +  80 bits data
    spi_clear_address(1);  // remove address
    spi_clear_command(1);  // remove command
    spi_clear_dummy(1);  // remove dummy
}

