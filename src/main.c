/*
 * Copyright 2021 ACES.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file main.c */
/* standard */
#include <string.h>

/* third party libs */
#include <FreeRTOS.h>
#include <task.h>
#include <espressif/esp_common.h>
#include <espressif/user_interface.h>
#include <esp/uart.h>
#include <pwm.h>
#include <httpd/httpd.h>

/* third party local libs */
#include <log.h>

/* local libs */
#include <ssi_utils.h>
#include <retval.h>

/* callbacks/cgi */
#include <logger_cgi.h>
#include <test_cgi.h>
#include <resistance_cgi.h>

/* local tasks */
#include <adc_reader.h>
#include <pwm_writer.h>
#include <uart_publisher.h>
#include <potentiometer_dwarf.h>

/* configuration includes */
#include <private_ssid_config.h>
#include <pinout_configuration.h>

/* global variable: between 0 and 1 */
uint8_t SYSTEM_LOG_LEVEL = LOG_INFO;

/**
 ** \brief   HTTP server task.
 **
 **  Sets mutexes, maps HTTP uris, sets handlers and callbacks (SSI, CGI and websockets).
 **
 ** \param   *pvParameters: The TCP socket.
 **/
void httpd_task(void *pvParameters) {
    tCGI pCGIs[] = {
        {"/test", (tCGIHandler)test_cgi_handler},
        {"/test/resource", (tCGIHandler)test_resource_cgi_handler},
        {"/test/parent_resource/child_a", (tCGIHandler) test_child_a_resource_cgi_handler},
        {"/resistance/value", (tCGIHandler) resistance_value_cgi_handler},
        {"/logger/level", (tCGIHandler)logger_level_cgi_handler},
    };

    /**
     * \note On SSI tags.
     *      Only use one SSI, the handler will just replace
     *      the tag with the string from a pointer+len. The
     *      function set_ssi_response() should be called
     *      before invoking the SSI enabled file. */
    const char *pcConfigSSITags[] = {
        "response",
    };

    /* register handlers and start the server */
    http_set_cgi_handlers(pCGIs, sizeof(pCGIs) / sizeof(pCGIs[0]));
    http_set_ssi_handler((tSSIHandler)ssi_handler, pcConfigSSITags,
            sizeof(pcConfigSSITags) / sizeof(pcConfigSSITags[0]));

    /* register handlers and start the server */
    httpd_init();
    for (;;) {
    vTaskDelay(10000 / portTICK_PERIOD_MS);
    }
}

/**
 * \brief Program entrypoint.
 */
void user_init(void) {
    uart_set_baud(0, 115200);
    log_set_level(SYSTEM_LOG_LEVEL);
    log_info("SDK version:%s ", sdk_system_get_sdk_version());
    struct sdk_station_config config = {
        .ssid = WIFI_SSID,
        .password = WIFI_PASS,
    };

    struct ip_info station_ip;

    /**
     * \todo The IP address set should be more automatic.
     **/
    IP4_ADDR(&station_ip.ip, 192, 168, 100, 20);
    IP4_ADDR(&station_ip.gw, 192, 168, 100, 1);
    IP4_ADDR(&station_ip.netmask, 255, 255, 255, 0);
    sdk_wifi_station_dhcpc_stop();
    vTaskDelay(50);

    /* required to call wifi_set_opmode before station_set_config */
    sdk_wifi_set_opmode(STATION_MODE);
    log_trace("static ip set status : %d", sdk_wifi_set_ip_info(STATION_IF, &station_ip));
    sdk_wifi_station_set_config(&config);
    sdk_wifi_station_connect();

    /* turn off LED */
    gpio_enable(ONBOARD_LED_PIN, GPIO_OUTPUT);
    gpio_write(ONBOARD_LED_PIN, true);

    /* initialize tasks */
//    xTaskCreate(&adc_read, "adc read", 256, NULL, 2, NULL);
//    xTaskCreate(&uart_publisher, "uart publisher", 256, NULL, 2, NULL);
//    xTaskCreate(&PWM_writer, "PWM_writer", 256, NULL, 2, NULL);
//    xTaskCreate(&spi_res, "spi_res", 256, NULL, 2, NULL);
    xTaskCreate(&httpd_task, "HTTP Daemon", 256, NULL, 2, NULL);
}
